﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArtSplit : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool useUpdate = false;

    [Header("Input ArtSplitAll")]
    public MeshFilter mf = null;

    [ContextMenu("ArtSplitAll")]
    public void ArtSplitAll()
    {
        if (isDebug) Debug.Log("ArtSplitAll");
        Transform trMFHolder = transform.Find("goMFHolder");

        if (trMFHolder != null)
            if (Application.isPlaying) Destroy(trMFHolder.gameObject); else DestroyImmediate(trMFHolder.gameObject);

        GameObject goMFHolder = new GameObject();
        goMFHolder.name = "goMFHolder";
        goMFHolder.transform.parent = this.transform;

        Mesh m = new Mesh();
        if (Application.isPlaying) m = mf.mesh; else m = mf.sharedMesh;

        List<Mesh> meshList = new List<Mesh>();
        for (int i = 0; i < m.triangles.Length / 3; i++)
        {
            Mesh newMesh = new Mesh();
            newMesh.vertices = new Vector3[3] { m.vertices[m.triangles[i * 3]], m.vertices[m.triangles[i * 3 + 1]], m.vertices[m.triangles[i * 3 + 2]] };
            newMesh.triangles = new int[3] { 0, 1, 2 };
            newMesh.RecalculateBounds();
            newMesh.RecalculateNormals();
            newMesh.RecalculateTangents();

            GameObject newArtGO = new GameObject();
            newArtGO.name = m.triangles[i] + "_" + m.triangles[i + 1] + "_" + m.triangles[i + 2];
            newArtGO.transform.position = (newMesh.vertices[0] + newMesh.vertices[1] + newMesh.vertices[2]) / 3;
            newArtGO.transform.parent = goMFHolder.transform;
            AnArt newArt = newArtGO.AddComponent<AnArt>();

            GameObject newMeshGO = new GameObject();
            newMeshGO.name = m.triangles[i] + "_" + m.triangles[i + 1] + "_" + m.triangles[i + 2];
            newMeshGO.transform.parent = newArtGO.transform;
            MeshFilter newMF = newMeshGO.AddComponent<MeshFilter>();
            newMF.sharedMesh = newMesh;
            MeshRenderer newMR = newMeshGO.AddComponent<MeshRenderer>();
            newMR.material = Application.isPlaying ? mf.GetComponent<MeshRenderer>().material : mf.GetComponent<MeshRenderer>().sharedMaterial;
        }
    }

    [Header("Input DragAll")]
    List<AnArt> artList = null;
    public int meshListCount = 0;

    [ContextMenu("GetArtList")]
    public void GetArtList()
    {
        artList = transform.GetComponentsInChildren<AnArt>().ToList();
        meshListCount = artList.Count;
    }

    [Header("Input DragAll")]
    public float disDrag = .5f;

    [ContextMenu("DragAll")]
    public void DragAll()
    {
        if (isDebug) Debug.Log("DragAll");
        if (artList == null || artList.Count == 0) GetArtList();
        for (int i = 0; i < artList.Count; i++)
        {
            MeshFilter mf = artList[i].GetComponentInChildren<MeshFilter>();
            Mesh m = Application.isPlaying ? mf.mesh : mf.sharedMesh;
            Vector3 v1 = m.vertices[0] - m.vertices[1];
            Vector3 v2 = m.vertices[0] - m.vertices[2];
            artList[i].transform.Translate(Vector3.Cross(v1, v2).normalized * disDrag);
        }
    }

    [ContextMenu("RotateAll")]
    public void RotateAll()
    {
        if (isDebug) Debug.Log("RotateAll");
        if (artList == null || artList.Count == 0) GetArtList();
        for (int i = 0; i < artList.Count; i++)
        {
            artList[i].transform.forward = Camera.main.transform.forward;
        }
    }

    private void OnEnable()
    {
        //CameraFlow camFlow = Camera.main.GetComponent<CameraFlow>();
        //camFlow.delafterUpdate += RotateAll;
    }

    private void FixedUpdate()
    {
        if (!useUpdate) return;
        RotateAll();
    }
}
