﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArtSplitMethod : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool useUpdate = false;

    [Header("Params")]
    ArtSplit _artSplit = null;
    ArtSplit artSplit { get { if (_artSplit == null) _artSplit = FindObjectOfType<ArtSplit>(); return _artSplit; } }

    [ContextMenu("Split")]
    public void Split()
    {
        artSplit.ArtSplitAll();
    }

    [ContextMenu("ChangeArtUpdate")]
    public void ChangeArtUpdate()
    {
        artSplit.useUpdate = !artSplit.useUpdate;
    }

    [ContextMenu("ShowArtInfo")]
    public void ShowArtInfo()
    {
        if (isDebug) Debug.Log("Parent : " + artSplit.transform.parent);
        if (isDebug) Debug.Log("Position: " + artSplit.transform.position);
    }
}
