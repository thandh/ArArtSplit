﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFlow : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    public Transform target;

    public float smoothSpeed = 0.125f;

    public Vector3 offSet;
    public float rotateSpeed;

    private float speedMod = 10.0f;//a speed modifier
    private Vector3 point;//the coord to the point where the camera looks at

    public delegate void delAfterUpdate();
    public delAfterUpdate delafterUpdate = null;

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(1) == true)
        {
            //Set up things on the start method
            point = target.transform.position;//get target's coords
            transform.LookAt(point);//makes the camera look to it
                                    //makes the camera rotate around "point" coords, rotating around its Y axis, 4 degrees per second times the speed modifier
            transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), 10 * Time.deltaTime * speedMod);
            if (isDebug) Debug.Log("mouse right down");
        }
        else if (Input.GetMouseButton(0) == true)
        {
            //Set up things on the start method
            point = target.transform.position;//get target's coords
            transform.LookAt(point);//makes the camera look to it
                                    //makes the camera rotate around "point" coords, rotating around its Y axis, 4 degrees per second times the speed modifier
            transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), -10 * Time.deltaTime * speedMod);
            if (isDebug) Debug.Log("mouse left down");
        }
        else
        {
            //Follow Player
            //Vector3 desiredPosition = target.position + offSet;
            //Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            //transform.position = smoothedPosition;
            //transform.LookAt(target);
        }

        if (delafterUpdate != null) delafterUpdate.Invoke();
    }

}