﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTestMethod : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool useUpdate = false;

    [Header("Params")]
    CubeTest _cube = null;
    CubeTest cube { get { if (_cube == null) _cube = FindObjectOfType<CubeTest>(); return _cube; } }

    [ContextMenu("ShowCubeInfo")]
    public void ShowCubeInfo()
    {
        if (isDebug) Debug.Log("Parent : " + cube.transform.parent);
        if (isDebug) Debug.Log("Position: " + cube.transform.position);
    }
}
